require('dotenv').load();

const express = require('express')
const mysql = require('promise-mysql');
const path = require('path');

const app = express();
app.use(express.static(path.join(__dirname, 'build')));

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });
  

var connexion;
var fields = [];

function getAllFields(conn) {
    conn.query('DESCRIBE census_learn_sql')
        .then(result => {
            fields = result.map(line => line.Field).filter(field => field !== "age")
        });
}

mysql.createConnection({
    host: process.env.MYSQL_HOST,
    port: process.env.MYSQL_PORT,
    user: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASSWORD,
    database: process.env.MYSQL_DATABASE
})
.then(conn => {
    connexion = conn;
    getAllFields(conn);
});

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'build', 'index.html'));
});

app.get('/api/fields', (req, res) => {
    res.json(fields);
})

app.get('/api/:field', (req, res) => {
    const field = req.params.field;
    if(!field || fields.indexOf(field) === -1) {
        res.status(400).end();
    };
    console.log("Querying database...");
    connexion.query(
        `SELECT \`${field}\` AS field, COUNT(*) AS count, ROUND(AVG(age), 1) AS average_age FROM census_learn_sql GROUP BY \`${field}\`;`
    ).then(result => {
        console.log("Sending result to client");
        res.json(result);
    }).catch(e => {
        console.error("Got error while querying database :");
        console.error(e);
        res.status(500).end();
    });
});


app.listen(process.env.PORT || 8000, () => {
    console.log("Listening on port 8000");
});