import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';


import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import selectorReducer from './reducers/selector-reducer';
import tableReducer from './reducers/table-reducer';

const allStoreEnhancers = compose(
    applyMiddleware(thunk),
    window.devToolsExtension && window.devToolsExtension()
);


const allReducers = combineReducers({
    selector: selectorReducer,
    table: tableReducer
});

const store = createStore(
    allReducers,
    {},
    allStoreEnhancers
);





ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));
registerServiceWorker();
