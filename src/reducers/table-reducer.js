import { TABLE_FETCHED } from '../actions/table-actions';

export default (state = [], action) => {
    switch(action.type) {
        case TABLE_FETCHED:
            return action.payload
        default:
            return state;
    }
}