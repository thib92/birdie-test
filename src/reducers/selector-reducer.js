import { UPDATE_SELECTION } from '../actions/selector-actions';
import { FIELDS_FETCHED } from '../actions/selector-actions';
import { TABLE_FETCHED } from '../actions/table-actions';

const defaultState = {
    selection: '',
    fields: [],
    loading: false
}

export default (state = defaultState, action) => {
    switch(action.type) {
        case UPDATE_SELECTION:
            return {
                ...state,
                selection: action.payload,
                loading: true
            };
        case FIELDS_FETCHED:
            return {
                ...state,
                fields: action.payload
            }
        case TABLE_FETCHED:
            return {
                ...state,
                loading: false
            }
        default:
            return state;
    }
}