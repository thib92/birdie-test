import React, { Component } from 'react';
import './App.css';
import Selector from './components/Selector';
import Table from './components/Table';

class App extends Component {

  render() {

    return (
      <div>
        <Selector />
        <Table />
      </div>
    );
  }
}

export default App;
