import axios from 'axios';

export const UPDATE_SELECTION = 'selection.change_selector';
export const FIELDS_FETCHED = 'selection.fields_fetched';

export function updateSelection(selection) {
    return {
        type: UPDATE_SELECTION,
        payload: selection
    }
}

export function requestFields() {
    return dispatch => {
        axios.get(encodeURI('/api/fields'))
            .then(res => dispatch({
                type: FIELDS_FETCHED,
                payload: res.data
            })
        )
    }
}