import axios from "axios";

export const TABLE_FETCHED = "table.fetched";

export function apiRequest(selection) {
    return dispatch => {
        axios.get(encodeURI('/api/'+selection))
            .then(res => dispatch({
                type: TABLE_FETCHED,
                payload: res.data
            })
        )
    }
}