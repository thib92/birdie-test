import React, { Component } from 'react';

import { connect } from 'react-redux';

import { updateSelection } from '../actions/selector-actions';
import { apiRequest } from '../actions/table-actions';
import { requestFields } from '../actions/selector-actions';

import { createSelector } from 'reselect';

class Selector extends Component {

    constructor(props) {
        super(props);
        this.onUpdateSelection = this.onUpdateSelection.bind(this);
    }

    componentDidMount() {
        this.props.onRequestFields();
    }

    onUpdateSelection(e) {
        this.props.onUpdateSelection(e.target.value);
        this.props.onApiRequest(e.target.value);
    }

    render() {
        const options = this.props.fields.map((field, index) => (<option key={index} value={field}>{field}</option>));
        return (
            <div>
                <div className="form-group">
                    <label>Select a field</label>
                    <select onChange={this.onUpdateSelection} className="form-control">
                        {options}
                    </select>
                </div>
            </div>
        );
    }

}

const mapStateToProps = createSelector(
    state => state.selector,
    selector => selector
);

const mapActionsToProps = {
    onUpdateSelection: updateSelection,
    onApiRequest: apiRequest,
    onRequestFields: requestFields
};

export default connect(mapStateToProps, mapActionsToProps)(Selector);