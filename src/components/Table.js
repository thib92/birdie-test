import React, { Component } from 'react'

import { connect } from 'react-redux';
import { createSelector } from 'reselect';


const TableRow = (props) => {
  return (
    <tr>
        <td>{props.id+1}</td>
        <td>{props.elem.field}</td>
        <td>{props.elem.count}</td>
        <td>{props.elem.average_age}</td>
    </tr>
  )
}



class Table extends Component {
    render() {
        if(this.props.selector.loading) return <div className="loader text-center"></div>;
        if(!this.props.table || this.props.table.length === 0) return null;


        const list =
            this.props.table
            .slice(0, Math.min(100, this.props.table.length))
            .map((elem, index) => (<TableRow key={index} elem={elem} id={index} />))
        ;
        
        return (
            <div>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>{this.props.selector.selection}</th>
                            <th>Count</th>
                            <th>Age</th>
                        </tr>
                    </thead>
                    <tbody>
                        {list}
                    </tbody>
                </table>
                <p>Total number of entries : {this.props.table.length}</p>
            </div>
        )
    }
}

const tableSelector = createSelector(
    state => state.table,
    table => table
);
const selectorSelector = createSelector(
    state => state.selector,
    selector => selector
);

const mapStateToProps = createSelector(
    tableSelector,
    selectorSelector,
    (table, selector) => ({table, selector})
);

const mapActionsToProps = {};

export default connect(mapStateToProps, mapActionsToProps)(Table);